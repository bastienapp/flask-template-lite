from flask import Flask
from flask_mysqldb import MySQL
from dotenv import load_dotenv
app = Flask(__name__)

load_dotenv()

app.config.from_object('config')

mysql = MySQL(app)

if __name__ == '__main__':
    import os
    port = int(os.getenv('PORT')) if os.getenv('PORT') else 5000
    from waitress import serve
    serve(app, host="0.0.0.0", port=port)
